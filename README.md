# Brightspace REST API

[Software Artifact Page](https://gojira.its.maine.edu/confluence/display/WB/Brightspace+REST+API)

[Brightspace](https://www.d2l.com/products/learning-environment/) Learning Management System REST API

This REST API sits on top of the actual Brightspace API and encapsulates the logic needed to accomplish tasks in the Brightspace environment. For example, we have a need to create a sandbox course for every faculty member. This involves creating the course itself (if it doesn't already exist), copying in some default content and enrolling that user as an instructor into that course. All that we need to provide to this API is their Org Defined ID.

## Note to the Public

This is written and maintained for the use of the [University of Maine System](https://www.maine.edu/). Feature requests will likely not be acted on. Merge requests will be reviewed, but there is no guarantee that they will be accepted.

### Our Environment

We utilize a self hosted version of [Gitlab](https://about.gitlab.com/stages-devops-lifecycle/) Enterprise, backed by [Rancher](https://rancher.com/). Our CI is [Auto DevOps](https://about.gitlab.com/stages-devops-lifecycle/auto-devops/), which requires zero configuration to build and deploy the application to the kubernetes clusters. Git branching is in the standard [GitFlow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow). This repository that you are seeing is a [mirror](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html) of our private repository.

## Development

Set NODE_ENV in .env to `development`.
If you need to change the server port, which defaults to 8000, set `PORT_HTTPS` in .env.
You will need to set `APP_HOST` in .env to your hostname.
To run the development server with http (`PORT` in .env, which defaults to 5000), `yarn dev`. With https (you'll need a cert), which should only be needed if you need to obtain the user_id and user_key from the authentication route, `yarn dev-https`.
For https, you will need a certificate, `/fullchain.pem` and private key, `/privkey.pem`. If you don't already have them, you can generate them using [certbot](https://certbot.eff.org/).
The LMS API will post back to https://[APP_HOST]:[PORT_HTTPS], so make sure that this port is accessible to the outside world.
If you want to use the Intellij debugger, you should use `yarn start`, just know that you will have to restart the debugger to see any code changes you make.

If you would like to run this against the BrightSpace provided public instance, set your .env to:

```
LMS_HOST=https://devcop.brightspace.com
LMS_PORT=443
LMS_APP_ID=31brpbcCLsVim_K4jJ8vzw
LMS_APP_KEY=sagYSTT_HOts39qrGQTFWA
LMS_USER_ID=1zzQYPR1hR49hGwCTPwCAc
LMS_USER_KEY=-NivbEqpvbOiTLZHiaJMRm
```

### Mock Server

A [mock server](https://www.npmjs.com/package/mockserver) is provided to mock the LMS API. Use `yarn mock-server` to start it up. It will be running at the environment variable, MOCK_PORT, or 9001. Credentials are not needed or even examined. Only the following environment variables are needed for the application to connect to the mock server.

```
LMS_HOST=http://localhost
LMS_PORT=[MOCK_PORT|9001]
```

## Testing

Specification/Documentation of the API uses [api blueprint](https://apiblueprint.org/).
Run `yarn test` to run [Dredd](https://dredd.org/en/latest/), which uses the blueprint to test the API.
[Dredd](https://dredd.org/en/latest/) will start up a copy of the application and the [mock server](#mock-server) to test against, then stop them afterwards.

If you would prefer to use [Swagger](https://swagger.io/), blueprint can be converted by running `yarn swagger`.

## Production

Run `docker-compose up -d`.
CI builds the image, but if you would like to build the app from source, run `docker-compose build`.
To use the CI built image, `docker-compose pull`.

## API Tokens

To generate a jwt token to be used by an application, run `yarn token` and answer the questions.

## Brightspace LMS API

Brightspace is also known as D2L or Desire2Learn.

### Useful links

- [API Reference](https://docs.valence.desire2learn.com/reference.html).
- [Example creating user](<https://docs.valence.desire2learn.com/res/user.html#post--d2l-api-lp-(version)-users->).
- [Code Repo for SDKs, utilities and examples](https://brightspace.github.io/).
- [Developer Community Discussions](https://community.brightspace.com/s/topic/0TO610000000JcwGAE/developer).
  - This has many more useful examples and overall great place for obtaining documentation on how to do something.
  - [Headless Web Service Workflow](https://community.brightspace.com/s/article/API-Cookbook-Headless-Non-Interactive-Web-Service-Workflow).

### Rate Limiting

BrightSpace API is rate limited. The current rate is 50,000 credits within 60 seconds, with each call currently costing 10 credits. A note of caution: while you may have credits in your favor, that doesn't mean you can't put enough load on the system to make it unresponsive. An example, if I ask to [get all children for a given org unit](<https://docs.valence.desire2learn.com/res/orgunit.html#get--d2l-api-lp-(version)-orgstructure-(orgUnitId)-children->) that has quite a few children, I will be using quite a bit of resources as opposed to [enrolling a learner into a course](<https://docs.valence.desire2learn.com/res/enroll.html#post--d2l-api-lp-(version)-enrollments->). If multiple queries to discover children are done in rapid succession, then you may see a decrease in performance, all while having an abundance of credits available. You can take the more advisable route of getting the same result of [children of an organization in pages](<https://docs.valence.desire2learn.com/res/orgunit.html#get--d2l-api-lp-(version)-orgstructure-(orgUnitId)-children-paged->). However, while this means you will be using more credits to accomplish the same dataset, thus giving you a more accurate representation of actual usage of the system, it still does not let you know if you are using resources in a manner that would be detrimental. More information can be found on the BrightSpace Community page, [API Rate Logging/Limiting - FAQ](https://community.brightspace.com/s/article/API-Rate-Logging-Limiting-FAQ)

TL;DR
Rate limiting alone should not be used to ensure the load you are sending through the API is not causing a negative effect on system responsiveness.

#### Environment Variables

| Variable                     | Description                                                | Value               |
| ---------------------------- | ---------------------------------------------------------- | ------------------- |
| LMS_RATE_LIMIT_MAX           | Maximum number of calls allowed                            | 5,000 (50,000 / 10) |
| LMS_RATE_LIMIT_TIME          | Time, in milliseconds, to count credits                    | 60,000              |
| LMS_RATE_LIMIT_REMAIN_HEADER | lowercase header name that tells how many credits are left |

## LMS Credentials

### Application

The `LMS_APP_ID` and `LMS_APP_KEY` can be obtained by going to [Brightspace](https://courses.maine.edu), formally or directly if needed [Brightspace direct](https://maine.brightspace.com). Under settings gear icon > Manage Extensibility > UMS ETL, You should see an Application ID `LMS_APP_ID` and Application Key `LMS_APP_KEY`.

### User

The `LMS_USER_ID` and `LMS_USER_KEY` have to be obtained from the API. This can be done by running their API Test Tool [locally](https://github.com/Brightspace/util-api-test-tool), or [online](https://apitesttool.desire2learnvalence.com/index.php). Also you can also use any of the [examples](https://docs.valence.desire2learn.com/clients/index.html) that utilize their SDK. Regardless of the method used to authenticate to their API, the callback url (the place where you are running the application) needs to be listed as the Trusted URL in the Settings > Manage Extensibility > [application name]. The callback url also needs to be publicly available so that Brightspace can send the credentials to that place. It is _HIGHLY_ recommended that this be secured with SSL. After authenticating, using a service account, use the `x_a` parameter as the `LMS_USER_ID` and the `x_b` parameter as the `LMS_USER_KEY`. For environments that SSO has been enabled, make sure that you login first using [environment url]/d2l/local. When using the same browser session, the service account user should be logged into the system and only ask for permission.

#### Expiration

The user tokens received back from the Brightspace API have a timeout of 30 days. As longs as there is a call every 30 days, these tokens will work (according to the [documentation](https://community.brightspace.com/s/article/API-Cookbook-Headless-Non-Interactive-Web-Service-Workflow)).
