function numToTwoDigitString(number) {
	return String(number).padStart(2, "0");
}

exports.fullDateString = function () {
	let today = new Date();
	let yy = today.getFullYear();
	let mm = numToTwoDigitString(today.getMonth() + 1);
	let dd = numToTwoDigitString(today.getDate());
	let hh = numToTwoDigitString(today.getHours());
	let mi = numToTwoDigitString(today.getMinutes());
	let ss = numToTwoDigitString(today.getSeconds());
	let ms = numToTwoDigitString(today.getMilliseconds());
	return `${yy}${mm}${dd}${hh}${mi}${ss}${ms}`;
};
