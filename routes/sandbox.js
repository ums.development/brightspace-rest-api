const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const userController = require("../controllers/user");
const enrollmentController = require("../controllers/enrollment");
const logger = require("../services/logger");

router.post("/", function (req, res) {
	const userId = req.body.userId;
	const courseId = "sb_" + userId;
	const templateId = process.env.LMS_SANDBOX_TEMPLATE_ID;

	let ids = {
		course: null,
		user: null,
		enrollment: null,
		copyJobToken: null,
		previewUser: null,
	};

	courseController.isCoursePresent(templateId, courseId).then(function (id) {
		if (id !== null) {
			logger.info("Sandbox-Post already exists", {
				route: "sandbox",
				method: "post",
				result: "exists",
				userId: userId,
				id,
			});
			ids.course = id;

			userController
				.isUserPresent(userId)
				.then(function (userLmsId) {
					ids.user = userLmsId;
					return createPreviewUserIfNeeded(userId);
				})
				.then(function () {
					return enrollmentController.add({
						type: "I",
						courseLmsId: ids.course,
						userLmsId: ids.user,
					});
				})
				.then(function () {
					res.status(208).json({ id: ids.course, code: courseId });
				})
				.catch(function (err) {
					res.status(400).json({
						errors: err,
					});
				});
		} else {
			courseController
				.addSandboxCourse(templateId, userId, courseId)
				.then(function SetCourseId(lmsCourseData) {
					ids.course = lmsCourseData.id;
				})
				.then(function SetCourseAsInactive() {
					return courseController.setCourseAsInactive(ids.course);
				})
				.then(function () {
					return courseController.copyCourse(
						process.env.LMS_SANDBOX_MASTER_COURSE_ID,
						ids.course
					);
				})
				.then(function (copyJobToken) {
					ids.copyJobToken = copyJobToken;
				})
				.then(function GetUserId() {
					return userController.getUserId(userId);
				})
				.then(function (userLmsId) {
					ids.user = userLmsId;
					return userLmsId;
				})
				.then(function (userLmsId) {
					return createPreviewUserIfNeeded(userLmsId);
				})
				.then(function (previewUserLmsId) {
					ids.previewUser = previewUserLmsId;
				})
				.then(function EnrollInstructor() {
					const enrollmentData = {
						userLmsId: ids.user,
						courseLmsId: ids.course,
						type: "I",
					};

					return enrollmentController.add(enrollmentData);
				})
				.then(function (enrollData) {
					ids.enrollment = enrollData;
				})
				.then(function () {
					logger.info("Sandbox-Post success: ", {
						route: "sandbox",
						method: "post",
						result: "success",
						...ids,
					});
					res.status(200).json({ id: ids.course, code: courseId });
				})
				.catch(function (err) {
					logger.warn("Sandbox-Post error: ", {
						reqBody: req.data,
						error: err,
						route: "sandbox",
						method: "post",
						result: "error",
						...ids,
					});
					res.status(400).json({
						errors: err,
					});
				});
		}
	});
});

function createPreviewUserIfNeeded(userId) {
	return new Promise(function (resolve, reject) {
		let previewUserId = userId + process.env.PREVIEW_USER_POSTFIX;

		userController
			.isUserPresent(previewUserId)
			.then(function (previewUserLmsId) {
				if (previewUserLmsId === null) {
					let previewUserData = {
						userId: previewUserId,
						firstName: "~Preview",
						lastName: "~Learner",
						email: "d2l.demolearner@mailinator.com",
						username: previewUserId,
						primaryCampus: null,
						campuses: [],
					};

					return userController.add(previewUserData);
				} else {
					return previewUserLmsId;
				}
			})
			.then(function (previewUserLmsId) {
				return enrollmentController.addSystemEnrollment(
					process.env.LMS_UMS_ORG_ID,
					previewUserLmsId,
					process.env.LMS_PREVIEW_USER_ROLE_ID
				);
			})
			.then(function (enrollmentData) {
				resolve(enrollmentData.userId);
			})
			.catch(function (err) {
				reject(err);
			});
	});
}

module.exports = router;
