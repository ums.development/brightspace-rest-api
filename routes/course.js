const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const logger = require("../services/logger");

router.post("/copy-master/:id", async function (req, res) {
	const courseLmsId = req.params.id;
	const data = req.body;

	if (!data.hasOwnProperty("masterCourseCode")) {
		return res.status(400).json({
			errors: {
				error: "Course code missing",
			},
		});
	}
	const masterCourseCode = data.masterCourseCode;

	const isPresent = await courseController.isCoursePresentById(courseLmsId);
	if (!isPresent) {
		return res.status(404).json({
			errors: {
				error: "Course to copy into is missing",
			},
		});
	}

	const masterCourseTemplateId = process.env.LMS_MASTER_COURSE_TEMPLATE_ID;
	const masterCourseLmsId = await courseController.isCoursePresent(
		masterCourseTemplateId,
		masterCourseCode
	);
	if (!masterCourseLmsId) {
		return res.status(404).json({
			errors: {
				error: "Master Course to copy from is missing",
			},
		});
	}

	courseController
		.copyCourse(masterCourseLmsId, courseLmsId)
		.then(function (copyJobToken) {
			return res.status(200).json({
				masterCourseLmsId,
				masterCourseCode,
				courseLmsId,
				copyJobToken,
			});
		})
		.catch(function (error) {
			return res.status(400).json({
				errors: { error },
			});
		});
});

router.post("/", function (req, res) {
	const data = req.body;
	let ids = {
		course: null,
		copyJobToken: null,
	};

	const errs = courseController.getCourseErrors(true, data);
	if (errs.length > 0) {
		res.status(400).json({
			errors: errs,
		});
	} else {
		courseController.setDefaultCourseProperties(data);

		courseController
			.isCoursePresentByTemplate(data.parents[0], data.code)
			.then(function (id) {
				if (id !== null) {
					logger.info("Course-Post already exists", {
						id,
						code: data.code,
						route: "course",
						method: "post",
						result: "exists",
					});
					courseController
						.updateCourse(
							id,
							data.code,
							data.name,
							data.isActive,
							true,
							data.startDate,
							data.endDate
						)
						.then(function (ids) {
							logger.info("Course-Post success: ", {
								id,
								code: data.code,
								route: "course",
								method: "post",
								result: "success",
							});
							res.status(208).json({ id: id, code: data.code });
						})
						.catch(function (err) {
							logger.info("Course-Post error for course: ", {
								id,
								code: data.code,
								route: "course",
								method: "post",
								result: "error",
								error: err,
							});
							res.status(400).json({
								errors: err,
							});
						});
				} else {
					courseController
						.addCourse(data)
						.then(function SetCourseId(lmsCourseData) {
							ids.course = lmsCourseData.id;
						})
						.then(function SetCourseActiveStatus() {
							if (data.isActive === false) {
								return courseController.setCourseAsInactive(ids.course);
							}
						})
						.then(function () {
							return courseController.copyCourse(data.master, ids.course);
						})
						.then(function (copyJobToken) {
							ids.copyJobToken = copyJobToken;
						})
						.then(function () {
							logger.info("Course-Post success: ", {
								id: ids.course,
								code: data.code,
								route: "course",
								method: "post",
								result: "success",
							});
							res.status(200).json({ id: ids.course, code: data.code });
						})
						.catch(function (err) {
							logger.warn("Course-Post error: ", {
								id: ids.course,
								code: data.code,
								route: "course",
								method: "post",
								result: "error",
								reqBody: req.data,
								error: err,
							});
							res.status(400).json({
								errors: err,
							});
						});
				}
			});
	}
});

router.post("/dev", async function (req, res) {
	const data = req.body;
	const errs = courseController.getCourseErrors(false, data);
	if (errs.length > 0) {
		return res.status(400).json({
			errors: errs,
		});
	}

	let devCourseTemplateId = process.env.LMS_DEV_COURSE_TEMPLATE_ID;
	let isMaster = false;
	if (data.hasOwnProperty("isMaster") && String(data.isMaster) === "true") {
		devCourseTemplateId = process.env.LMS_MASTER_COURSE_TEMPLATE_ID;
		isMaster = true;
	}

	let id = await courseController.isCoursePresent(
		devCourseTemplateId,
		data.code
	);
	if (id !== null) {
		logger.info("Course-Dev-Post already exists", {
			id,
			code: data.code,
			route: "course-dev",
			method: "post",
			result: "exists",
		});
		return res.status(409).json({ id: id, code: data.code });
	}

	let ids = {
		course: null,
		copyJobToken: null,
	};
	courseController
		.addDevCourse(data, isMaster)
		.then(function SetCourseId(lmsCourseData) {
			ids.course = lmsCourseData.id;
		})
		.then(function () {
			if (data.master && data.master.length > 0) {
				return courseController.copyCourse(data.master, ids.course);
			} else {
				return null;
			}
		})
		.then(function (copyJobToken) {
			ids.copyJobToken = copyJobToken;
		})
		.then(function () {
			logger.info("Course-Dev-Post success: ", {
				id: ids.course,
				code: data.code,
				route: "course-dev",
				method: "post",
				result: "success",
			});
			res.status(200).json({ id: ids.course, code: data.code });
		})
		.catch(function (err) {
			logger.warn("Course-Dev-Post error: ", {
				id: ids.course,
				code: data.code,
				route: "course-dev",
				method: "post",
				result: "error",
				reqBody: req.data,
				error: err,
			});
			res.status(400).json({
				errors: err,
			});
		});
});

router.put("/:id", function (req, res) {
	let id = req.params.id;
	let course = req.body;
	let errs = courseController.getCourseErrors(false, course);

	if (errs.length > 0) {
		res.status(400).json({
			errors: errs,
		});
	} else {
		courseController.setDefaultCourseProperties(course);

		let cascadeChanges = true;
		if (
			course.hasOwnProperty("cascadeChanges") &&
			String(course.cascadeChanges) === "false"
		) {
			cascadeChanges = false;
		}

		courseController
			.updateCourse(
				id,
				course.code,
				course.name,
				course.isActive,
				cascadeChanges,
				course.startDate,
				course.endDate
			)
			.then(function (ids) {
				logger.info("Course-Put success: ", {
					id,
					code: course.code,
					route: "course",
					method: "put",
					result: "success",
				});
				res.status(200).json(ids);
			})
			.catch(function (err) {
				logger.info("Course-Put error for course: ", {
					id,
					code: course.code,
					route: "course",
					method: "put",
					result: "error",
					error: err,
				});
				res.status(400).json({
					errors: err,
				});
			});
	}
});

router.get("/by-template/:id", function (req, res) {
	let id = req.params.id;

	courseController
		.getCoursesByTemplate(id)
		.then(function (courses) {
			logger.info("Course-Get-By-Template success: ", {
				templateId: id,
				route: "course-by-template",
				method: "get",
				result: "success",
			});
			res.status(200).json({ courses: courses });
		})
		.catch(function (err) {
			logger.info("Course-Get-By-Template error: ", {
				route: "course-by-template",
				method: "get",
				result: "error",
				templateId: id,
				errors: err,
			});
			res.status(400).json({
				templateId: id,
				errors: err,
			});
		});
});

router.get("/:id", function (req, res) {
	let id = req.params.id;
	if (isNaN(id) || !Number.isInteger(Number(id))) {
		res.status(400).json({
			error: "Invalid parameter, id",
		});

		return;
	}

	courseController
		.getCourseData(id)
		.then(function (course) {
			logger.info("Course-Get-By-Id success: ", {
				route: "course",
				method: "get",
				result: "success",
				id,
			});
			res.status(200).json(course);
		})
		.catch(function (err) {
			logger.info("Course-Get-By-Id error: ", {
				id,
				route: "course",
				method: "get",
				result: "error",
				error: err,
			});
			res.status(400).json({
				id: id,
				errors: err,
			});
		});
});

router.get("/finalGrades/:id", function (req, res) {
	let id = req.params.id;
	if (isNaN(id) || !Number.isInteger(Number(id))) {
		res.status(400).json({
			error: "Invalid parameter, id",
		});

		return;
	}

	courseController
		.getCourseFinalGrades(id)
		.then(function (grades) {
			logger.info("Course-Final-Grades-Get-By-Id success: ", {
				route: "course/finalGrades",
				method: "get",
				result: "success",
				id,
			});
			res.status(200).json(grades);
		})
		.catch(function (err) {
			logger.info("Course-Final-Grades-Get-By-Id error: ", {
				id,
				route: "course/finalGrades",
				method: "get",
				result: "error",
				error: err,
			});
			res.status(400).json({
				id: id,
				errors: err,
			});
		});
});

router.get("/:id/grades/:columnId", function (req, res) {
	let id = req.params.id;
	let columnId = req.params.columnId;
	let errors = [];
	if (isNaN(id) || !Number.isInteger(Number(id))) {
		errors.push({
			error: "Invalid parameter, id",
		});
	}
	if (isNaN(columnId) || !Number.isInteger(Number(columnId))) {
		errors.push({
			error: "Invalid parameter, columnId",
		});
	}

	if (errors.length > 0) {
		logger.info(
			"Course-Grades client error for course " +
				id +
				" and column " +
				columnId +
				" ",
			{
				route: "course-grades",
				method: "get",
				result: "error",
				id,
				columnId,
				errorType: "client",
				error: errors,
			}
		);
		res.status(400).json(errors);

		return;
	}

	courseController
		.getCourseGradesByColumn(id, columnId)
		.then(function (grades) {
			logger.info(
				"Course-Grades success for course " +
					id +
					" and column " +
					columnId +
					" ",
				{
					route: "course-grades",
					method: "get",
					result: "success",
					id,
					columnId,
				}
			);
			res.status(200).json(grades);
		})
		.catch(function (err) {
			logger.info(
				"Course-Grades error for course " +
					id +
					" and column " +
					columnId +
					" ",
				{
					route: "course-grades",
					method: "get",
					result: "error",
					id,
					columnId,
					error: err,
				}
			);
			res.status(400).json(err);
		});
});

router.get("/:id/enrollments", function (req, res) {
	let id = req.params.id;
	if (isNaN(id) || !Number.isInteger(Number(id))) {
		res.status(400).json({
			error: "Invalid parameter, id",
		});

		return;
	}

	courseController
		.getCourseEnrollmentsWithRoleName(id)
		.then(function (enrollments) {
			logger.info("Course-Enrollments success for " + id, {
				route: "course-enrollments",
				method: "get",
				result: "success",
				id,
			});
			res.status(200).json(enrollments);
		})
		.catch(function (err) {
			logger.info("Course-Enrollments error: ", {
				id,
				error: err,
				route: "course-enrollments",
				method: "get",
				result: "error",
			});
			res.status(400).json({
				id: id,
				errors: err,
			});
		});
});

router.delete("/:id", function (req, res) {
	let id = req.params.id;
	if (isNaN(id) || !Number.isInteger(Number(id))) {
		res.status(400).json({
			error: "Invalid parameter, id",
		});

		return;
	}
	courseController
		.delete(id)
		.then(function (deletedCourse) {
			logger.info("Course-Delete removed course", {
				route: "course",
				method: "delete",
				result: "success",
				id,
				code: deletedCourse.Code,
			});
			res.status(200).json(deletedCourse);
		})
		.catch(function (err) {
			if (err.status === 404) {
				logger.info("Course-Delete non existent course", {
					route: "course",
					method: "delete",
					result: "nonexistent",
					id,
				});
			} else {
				//todo: error object doesn't contain status
				logger.info("Course-Delete errored ", {
					error: err,
					route: "course",
					method: "delete",
					result: "error",
					id,
				});
			}
			res.status(400).json({
				errors: err,
			});
		});
});
module.exports = router;
