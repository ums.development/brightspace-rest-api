const express = require("express");
const router = express.Router();
let rlValues = require("../helpers/axios").rateLimitValues;

router.get("/", function (req, res) {
	res.status(200).json({
		system: rlValues.appState(),
		"X-Rate-Limit-Remaining": rlValues.rlRemain(),
		"X-Rate-Limit-Reset": rlValues.rlReset(),
	});
});

module.exports = router;
