const express = require("express");
const router = express.Router();
const enrollmentController = require("../controllers/enrollment");
const courseController = require("../controllers/course");
const logger = require("../services/logger");

router.post("/", function (req, res) {
	enrollmentController
		.add(req.body)
		.then(function (resEnrollmentData) {
			logger.info("Enroll-Post enrolled user ", {
				...resEnrollmentData,
				route: "enrollment",
				method: "post",
				result: "success",
			});

			res.status(200).json(resEnrollmentData);
		})
		.catch(function (err) {
			logger.info("Enroll-Post error ", {
				route: "enrollment",
				method: "post",
				result: "error",
				reqBody: req.body,
				error: err,
			});
			res.status(400).json({
				errors: err,
			});
		});
});

router.delete("/", function (req, res) {
	enrollmentController
		.delete(req.body)
		.then(function (enrollmentData) {
			logger.info("Enroll-Delete removed enrollment", {
				...enrollmentData,
				route: "enrollment",
				method: "delete",
				result: "success",
			});
			res.status(200).json(enrollmentData);
		})
		.catch(function (err) {
			if (err.status === 404) {
				logger.info("Enroll-Delete non existent user/course", {
					reqBody: req.body,
					route: "enrollment",
					method: "delete",
					result: "nonexistent",
				});
			} else {
				logger.info("Enroll-Delete errored ", {
					route: "enrollment",
					method: "delete",
					result: "error",
					reqBody: req.body,
					error: err,
				});
			}
			res.status(400).json({
				errors: err,
			});
		});
});

router.get("/by-organization/:org_type/:user_id", function (req, res) {
	let user_id = req.params.user_id;
	if (isNaN(user_id) || !Number.isInteger(Number(user_id))) {
		res.status(400).json({
			error: "Invalid parameter, user id",
		});
	}
	let org_type = req.params.org_type;

	let org_type_id = -1;
	switch (org_type) {
		case "ums":
			org_type_id = process.env.LMS_UMS_TYPE_ID;
			break;
		case "university":
			org_type_id = process.env.LMS_UNIVERSITY_TYPE_ID;
			break;
		case "course":
			org_type_id = process.env.LMS_COURSE_TYPE_ID;
			break;
	}

	if (org_type_id === -1) {
		res.status(400).json({
			error:
				"Invalid parameter, organization type. Use one of:  ums, university, course",
		});
	}

	enrollmentController
		.getEnrollmentsByOrganizationType(org_type_id, user_id)
		.then(function (enrollmentData) {
			let enrolls = [];
			for (e in enrollmentData) {
				let enroll = {
					org_id: enrollmentData[e].OrgUnit.Id,
					org_name: enrollmentData[e].OrgUnit.Name,
					org_code: enrollmentData[e].OrgUnit.Code,
					role_id: enrollmentData[e].Role.Id,
					role_name: enrollmentData[e].Role.Name,
				};

				enrolls.push(enroll);
			}

			return enrolls;
		})
		.then(function (enrollmentData) {
			logger.info("Enroll-Get-By-Org success", {
				route: "enrollment-by-org",
				method: "get",
				result: "success",
				...enrollmentData,
			});
			res.status(200).json(enrollmentData);
		})
		.catch(function (err) {
			logger.info("Enroll-Get-By-Org error ", {
				route: "enrollment-by-org",
				method: "get",
				result: "error",
				reqBody: req.body,
				error: err,
			});

			res.status(400).json({
				errors: err,
			});
		});
});

module.exports = router;
