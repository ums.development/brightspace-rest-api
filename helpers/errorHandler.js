exports.handleError = function (error) {
	let errObject = { Error: error.message };
	if (error.response) {
		errObject = {
			status: error.response.status,
			text: error.response.statusText,
			message: error.message,
			body: error.response.data,
		};
	} else if (error.request) {
		errObject = { Error: "No response received", message: error.message };
	}

	return errObject;
};
